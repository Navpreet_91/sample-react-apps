import React, { Component } from "react";
import { choice } from "./helper";
import Coin from "./Coin";

class CoinContainer extends Component {
  static defaultProps = {
    coins: [
      {
        side: "heads",
        url: "https://jkscoinworld.com/wp-content/uploads/2018/05/2013-a.jpg",
      },
      {
        side: "tails",
        url: "https://en.numista.com/catalogue/photos/inde/2409-original.jpg",
      },
    ],
  };

  constructor(props) {
    super(props);
    this.state = {
      currCoin: null,
      nFlips: 0,
      nHeads: 0,
      nTails: 0,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  flipCoin(e) {
    const newCoin = choice(this.props.coins);
    this.setState((st) => {
      return {
        currCoin: newCoin,
        nFlips: st.nFlips + 1,
        nHeads: st.nHeads + (newCoin.side === "heads" ? 1 : 0),
        nTails: st.nTails + (newCoin.side === "tails" ? 1 : 0),
      };
    });
  }

  handleClick(e) {
    this.flipCoin();
  }
  render() {
    return (
      <div className="CoinContainer">
        <h1>Let's Flip a Coin!!</h1>
        {this.state.currCoin && <Coin info={this.state.currCoin} />}
        <button onClick={this.handleClick}>Flip Me!</button>
        <p>
          Out of {this.state.nFlips} flips, there have been {this.state.nHeads}{" "}
          and {this.state.nTails}.
        </p>
      </div>
    );
  }
}

export default CoinContainer;
