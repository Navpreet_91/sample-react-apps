import React, { Component } from "react";
import axios from "axios";
import "./JokeList.css";
import Joke from "./Joke";
import { v4 as uuidv4 } from "uuid";

class JokeList extends Component {
  static defaultProps = {
    numJokesToGet: 10,
  };
  //somehwre in the state arry to save jokes when chnages rerender whole bunch of jokes
  constructor(props) {
    super(props);
    this.state = {
      jokes: JSON.parse(window.localStorage.getItem("jokes") || "[]"),
      loading: false,
    };
    this.seenJokes = new Set(this.state.jokes.map((j) => j.text));
    this.handleClick = this.handleClick.bind(this);
  }
  /* componentDidMount() is invoked immediately after a component is mounted (inserted into the tree). 
Initialization that requires DOM nodes should go here. If you need to load data from a remote endpoint,
 this is a good place to instantiate the network request. */

  componentDidMount() {
    if (this.state.jokes.length === 0) this.getJokes();
  }

  async getJokes() {
    try {
      let jokes = [];
      while (jokes.length < this.props.numJokesToGet) {
        let res = await axios.get("https://icanhazdadjoke.com/", {
          headers: {
            Accept: "application/json",
          },
        });
        let newJoke = res.data.joke;
        if (!this.seenJokes.has(newJoke)) {
          jokes.push({ id: uuidv4(), text: newJoke, votes: 0 });
          // console.log(jokes)
        } else {
          console.log("Joke Duplicated");
          console.log(newJoke);
        }
      }
      this.setState(
        (st) => ({
          loading: false,
          jokes: [...st.jokes, ...jokes],
        }),
        () =>
          window.localStorage.setItem("jokes", JSON.stringify(this.state.jokes))
      );
      window.localStorage.setItem("jokes", JSON.stringify(jokes));
    } catch (e) {
      alert(e);
      this.setState({loading:false})
    }
  }

  handleVote(id, delta) {
    this.setState(
      (st) => ({
        jokes: st.jokes.map((j) =>
          j.id === id ? { ...j, votes: j.votes + delta } : j
        ),
      }),
      () =>
        window.localStorage.setItem("jokes", JSON.stringify(this.state.jokes))
    );
  }

  handleClick() {
    this.setState({ loading: true }, this.getJokes);
  }
  render() {
    if (this.state.loading) {
      return (
        <div className="JokeList-spinner">
          <i className="fas fa-8x fa-spinner fa-spin"></i>
          <h1 className="JokeList-title">Loading......</h1>
        </div>
      );
    }
    let jokes=this.state.jokes.sort((a,b)=>b.votes-a.votes)
    return (
      <div className="JokeList">
        <div className="JokeList-navbar">
          <h1 className="JokeList-title">Dad Jokes</h1>
          <img
            src="http://memecrunch.com/meme/ARPCV/what-a-joke/image.jpg"
            width="200"
            height="200"
            alt="emoji"
          />
          <button className="JokeList-button" onClick={this.handleClick}>
            More Jokes
          </button>
        </div>

        <div className="JokeList-jokes">
          {jokes.map((j) => (
            <Joke
              key={j.id}
              votes={j.votes}
              text={j.text}
              upvote={() => this.handleVote(j.id, 1)}
              downvote={() => this.handleVote(j.id, -1)}
            />
            // <div>{j.joke}-{j.votes}</div>
          ))}
        </div>
      </div>
    );
  }
}

export default JokeList;
