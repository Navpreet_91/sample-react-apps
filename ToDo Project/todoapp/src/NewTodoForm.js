import React, { Component, useState } from "react";
import {v4 as uuid} from "uuid";

class NewTodoForm extends Component {
  constructor(props) {
    super(props);
    this.state = { task: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit=this.handleSubmit.bind(this);
  }
  handleChange = (e) => {
    this.setState({
        [e.target.name]:e.target.value
    })
  };

  handleSubmit=(e)=>{
    e.preventDefault();
    this.props.createTodo({...this.state,id:uuid()});
    this.setState({task:''})

  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label htmlfor="newtodo">New Todo</label>
        <input
          id="newtodo"
          type="text"
          placeholder="Add Todo"
          name='task'
          value={this.state.task}
          onChange={this.handleChange}
        />
        <button>Add Todo</button>
      </form>
    );
  }
}
export default NewTodoForm;

// const NewTodoForm = (props) => {
//   const [data, newdata] = useState();

//   const handleChange = (e) =>
//     newdata({
//       ...data,
//       [e.currentTarget.name]: e.currentTarget.value,
//     });

//   const handleSubmit = (e) => {
//     props.createTodo(data);
//     newdata("");
//   };


//   return (
//     <form onSubmit={handleSubmit}>
//       <label htmlfor="newtodo">New Todo</label>
//       <input
//         id="newtodo"
//         type="text"
//         placeholder="Add Todo"
//         name="task"
//         onChange={handleChange}
//       />
//       <button>Add Todo</button>
//     </form>
//   );
// };
// export default NewTodoForm;
