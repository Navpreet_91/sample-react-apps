import sizes from "./sizes";
import bg from "./bg.svg";
export default {
    main: {
      // background: "linear-gradient(25deg, #FE6B8B 10%, #FF8E53 90%)",
      height: "100vh",
      display: "flex",
      alignItems: "flex-start",
      justifyContent: "center",
      backgroundColor: "#394bad",
      backgroundImage: `url("https://images.unsplash.com/photo-1513542789411-b6a5d4f31634?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8Y29sb3JzfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60")`,
      backgroundSize: "cover",
      overflow: "scroll"
    },
    container: {
      display: "flex",
      width: "50%",
      alignItems: "flex-start",
      flexDirection: "column",
      flexWrap: "wrap",
      [sizes.down("xl")]: {
        width: "80%"
      },
      [sizes.down("xs")]: {
        width: "75%"
      }
    },
    nav: {
      display: "flex",
      width: "100%",
      justifyContent: "space-between",
      alignItems:'center',
      "& a":{
        color:'black',
        fontSize:'1.2rem',
        fontWeight:'bold'
      }
  
    },
    palettes: {
      boxSizing: "border-box",
      width: "100%",
      display: "grid",
      gridTemplateColumns: "repeat(3, 30%)",
      gridGap: "2.5rem",
      [sizes.down("md")]: {
        gridTemplateColumns: "repeat(2, 50%)"
      },
      [sizes.down("xs")]: {
        gridTemplateColumns: "repeat(1, 100%)",
        gridGap: "1.4rem"
      }
    },
  };
  