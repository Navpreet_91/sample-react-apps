const styles={
    picker:{
      width:"100% !important",
      marginTop:"2rem",
    },
    addButton:{
      width:"100%",
      padding:"1rem",
      marginTop:"1rem",
      fontSize:"1.3rem"
    },
    colorName:{
      width:"100%",
    }
    }
export default styles;    