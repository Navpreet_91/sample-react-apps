import React, { Component } from "react";
import { Link } from "react-router-dom";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import "rc-slider/assets/index.css";
import Slider from "rc-slider";
import Snackbar from "@material-ui/core/Snackbar";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "@material-ui/core/IconButton";
import {withStyles} from "@material-ui/styles"
import styles from "./styles/NavbarStyles";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = { format: "hex", open: false };
    this.handleFormatChange = this.handleFormatChange.bind(this);
    this.handleClose=this.handleClose.bind(this);
  }
  handleFormatChange(e) {
    this.setState({ format: e.target.value,open:true });
    this.props.handleChange(e.target.value);
  }

  handleClose() {
    this.setState({ open: false });
  }
  render() {
    const { level, changeLevel,showLevelBar,classes } = this.props;
    const { format } = this.state;
    return (
      <header className={classes.Navbar}>
        <div className={classes.logo}>
          <Link to="/">Color Picker</Link>
        </div>
        {showLevelBar&& (
        <div>
        <span>Level:{level}</span>
        <div className={classes.slider}>
          <Slider
            defaultValue={level}
            min={100}
            max={900}
            step={100}
            onAfterChange={changeLevel}
          />
        </div>
      </div>
        )}

        <div className={classes.selectContainer}>
          <Select onChange={this.handleFormatChange} value={format}>
            <MenuItem value="hex">HEX- #ffffff</MenuItem>
            <MenuItem value="rgb">RGB- rgb(255,255,255)</MenuItem>
            <MenuItem value="rgba">RGBA- rgba(255,255,255,1.0)</MenuItem>
          </Select>
        </div>
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={this.state.open}
          autoHideDuration={6000}
          message={<span id="message-id">Color Format Changed!!</span>}
          ContentProps={{
            "aria-describedby": "message-id",
          }}
          onClose={this.handleClose}
          action={
            <IconButton size="small" aria-label="close" color="inherit" key="close">
              <CloseIcon fontSize="small" onClick={this.handleClose} />
            </IconButton>
          }
        />
      </header>
    );
  }
}
export default withStyles(styles)(Navbar);
