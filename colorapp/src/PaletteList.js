import React, { Component } from "react";
import MiniPalette from "./MiniPalette";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles/PaletteListStyles";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import CloseIcon from '@material-ui/icons/Close';
import CheckIcon from '@material-ui/icons/Check';

class PaletteList extends Component {
  constructor(props){
    super(props);
    this.state={open:false, deletingId:""}
    this.openDialog=this.openDialog.bind(this)
    this.closeDialog=this.closeDialog.bind(this)
    this.handleDelete = this.handleDelete.bind(this);
    this.goToPalette=this.goToPalette.bind(this)
  }
  openDialog(id){
    this.setState({open:true,deletingId:id})
  }

  closeDialog(){
    this.setState({open:false,deletingId:""})
  }

  goToPalette(id) {
    this.props.history.push(`/palette/${id}`);
  }

  handleDelete() {
    this.props.deletePalette(this.state.deletingId);
    this.closeDialog();
  }
  render() {
    const { palettes, classes } = this.props;
    const {open}=this.state;
    return (
      <div className={classes.main}>
        <div className={classes.container}>
          <nav className={classes.nav}>
            <h1>React Colors Palettes</h1>
            <Link to="/palette/new">Create New Palette</Link>
          </nav>

          <div className={classes.palettes}>
            {palettes.map((p) => {
              return (
                <div>
                  <MiniPalette
                    {...p}
                    goToPalette={this.goToPalette}
                    // handleDelete={deletePalette}
                    openDialog={this.openDialog}
                    key={p.id}
                    id={p.id}
                  />
                </div>
              );
            })}
          </div>
        </div>
        <Dialog open={open} aria-labelledby="delete-dialog-title" onClose={this.closeDialog}>
          <DialogTitle id="delete-dialog-title">
            Are you sure,you want to delete this palette?
          </DialogTitle>
          <List>
  
          <ListItem  button onClick={this.handleDelete}>
            <ListItemAvatar>
              <Avatar className={classes.avatar} style={{backgroundColor:'lightblue',color:'blue'}}>
                <CheckIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText  primary="Delete"/>
          </ListItem>

        <ListItem button onClick={this.closeDialog}>
          <ListItemAvatar>
            <Avatar className={classes.avatar} style={{backgroundColor:'#F3BBAF',color:'red'}}>
              <CloseIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary="Cancel" />
        </ListItem>
      </List>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(PaletteList);
