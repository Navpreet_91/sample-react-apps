//import modules
const express = require("express");
const app = express();
const port = process.env.PORT || 5000;
const cors = require("cors");

//import JSON files
const data =require("./data");

app.use(cors());

//app listening to port for backend server
app.listen(port, 
    () => console.log(`Listening to port ${port}`));

//setting up routes to server for API
app.get("/first", (req, res) => {
    res.json(data);
    });

app.get("/", (req, res) => {
//   res.send({ message: "Its working!!!" });
  res.json({ message: "Its working!!!" });
});