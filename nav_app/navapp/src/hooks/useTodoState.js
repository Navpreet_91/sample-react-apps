import useLocalStorageState from "./useLocalStorageState";
import { v4 as uuidv4 } from "uuid";

export default (data) => {
  const [todos, setTodos] = useLocalStorageState("todos",data);

  return {
    todos,
    addTodo: (newTodoText) => {
      setTodos([
        ...todos,
        { id: uuidv4(), task: newTodoText, completed: false },
      ]);
    },
    removeTodo: (todoId) => {
      //filter out removed todo
      const updatedTodos = todos.filter((todo) => todo.id !== todoId);
      setTodos(updatedTodos);
    },
    toggleTodo: (todoId) => {
      const updatedTodos = todos.map((todo) =>
        todo.id === todoId ? { ...todo, completed: !todo.completed } : todo
      );
      setTodos(updatedTodos);
    },

    editTodo: (todoId, newtask) => {
      const updatedTodos = todos.map((todo) =>
        todo.id === todoId ? { ...todo, task: newtask } : todo
      );
      setTodos(updatedTodos);
    },
  };
};

// const addTodo = (newTodoText) => {
//   setTodos([...todos, { id: uuidv4(), task: newTodoText, completed: false }]);
// };

// const removeTodo = (todoId) => {
//   //filter out removed todo
//   const updatedTodos = todos.filter((todo) => todo.id !== todoId);
//   setTodos(updatedTodos);
// };

// const toggleTodo = (todoId) => {
//   const updatedTodos = todos.map((todo) =>
//     todo.id === todoId ? { ...todo, completed: !todo.completed } : todo
//   );
//   setTodos(updatedTodos);
// };

// const editTodo = (todoId, newtask) => {
//   const updatedTodos = todos.map((todo) =>
//     todo.id === todoId ? { ...todo, task: newtask } : todo
//   );
//   setTodos(updatedTodos);
// };
