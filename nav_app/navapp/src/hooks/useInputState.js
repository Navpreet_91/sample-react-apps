import { useState } from "react";

const useStateHooks= (intialVal) => {
  const [value, setValue] = useState(intialVal);

  const handleChange = (e) => {
    setValue(e.target.value);
  };

  const reset = () => {
    setValue("");
  };

  return [value, handleChange, reset];
} ;

export default useStateHooks;