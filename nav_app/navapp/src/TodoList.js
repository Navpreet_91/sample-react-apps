import React,{useContext} from "react";
import { Paper, List, Divider } from "@material-ui/core";
import Todo from "./Todo";
import {TodosContext} from "./context/todos.context";

function TodoList() {
  const {todos} = useContext(TodosContext);
  if (todos.length)
    return (
      <Paper>
        <List>
          {todos.map((todo, index) => {
            return (
              <React.Fragment key={index}>
                <Todo {...todo} key={todo.id} />
                <Divider />
              </React.Fragment>
              //   <>
              // <ListItem>
              //   <ListItemText>{todo.task}</ListItemText>
              // </ListItem>
              // <Divider/>
              // </>
            );
          })}
        </List>
      </Paper>
    );
  return <h3>No todos...</h3>;
}

export default TodoList;
