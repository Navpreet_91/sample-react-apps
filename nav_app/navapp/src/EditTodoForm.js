import React,{useContext} from "react";
import { TextField } from "@material-ui/core";
import useInputState from "./hooks/useInputState";
import {DispatchContext} from "./context/todos.context";

function EditTodoForm({  id, task, toggleEditDone }) {
  const dispatch=useContext(DispatchContext)
  const [value, handleChange, reset] = useInputState(task);
  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        dispatch({type:"EDIT",id:id,task:value})
        reset();
        toggleEditDone();
      }}
      style={{marginLeft:'1rem',width:'100%'}}
    >
      <TextField
        value={value}
        onChange={handleChange}
        margin="normal"
        fullWidth
        autoFocus
      />
    </form>
  );
}

export default EditTodoForm;
