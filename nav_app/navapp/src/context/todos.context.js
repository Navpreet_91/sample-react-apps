import React, { createContext, useReducer } from "react";
import todoReducer from "../reducers/todo.reducer";
import useTodoState from "../hooks/useTodoState";

const defaultTodos = [
  { id: 1, task: "Feed a fish", completed: true },
  { id: 2, task: "Bath a cat", completed: false },
];

export const TodosContext = createContext();
export const DispatchContext = createContext();

export function TodosProvider(props) {
  const [todos, dispatch] = useReducer(todoReducer, defaultTodos);
  // const todosStuff = useTodoState(defaultTodos);
  return (
    <TodosContext.Provider value={{ todos }}>
      <DispatchContext.Provider value={{ dispatch }}>
        {props.children}
      </DispatchContext.Provider>
    </TodosContext.Provider>
  );
}
